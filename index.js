function createNewUser() {
    const newUser = {
      firstName: '',
      lastName: '',
      birthday: '',
      getLogin: function () {
        return `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`;
      },
      setFirstName: function (newFirstName) {
        if (typeof newFirstName === 'string') {
          this.firstName = newFirstName;
        } else {
          console.error('Invalid input. First name should be a string.');
        }
      },
      setLastName: function (newLastName) {
        if (typeof newLastName === 'string') {
          this.lastName = newLastName;
        } else {
          console.error('Invalid input. Last name should be a string.');
        }
      },
      setBirthday: function (newBirthday) {
        const datePattern = /^\d{2}\.\d{2}\.\d{4}$/;
        if (datePattern.test(newBirthday)) {
          this.birthday = newBirthday;
        } else {
          console.error('Invalid input. Birthday should be in the format dd.mm.yyyy.');
        }
      },
      getAge: function () {
        if (!this.birthday) {
          console.error('Birthday is not set.');
          return;
        }
  
        const today = new Date();
        const birthYear = parseInt(this.birthday.split('.')[2]);
        const currentYear = today.getFullYear();
        const age = currentYear - birthYear;
  
        const birthMonth = parseInt(this.birthday.split('.')[1]) - 1; 
        const currentMonth = today.getMonth();
        const birthDay = parseInt(this.birthday.split('.')[0]);
        const currentDay = today.getDate();
  
        if (currentMonth < birthMonth || (currentMonth === birthMonth && currentDay < birthDay)) {
          return age - 1;
        }
  
        return age;
      },
      getPassword: function () {
        if (!this.firstName || !this.lastName || !this.birthday) {
          console.error('Some user information is missing.');
          return;
        }
  
        const firstNameInitial = this.firstName.charAt(0).toUpperCase();
        const lastNameLower = this.lastName.toLowerCase();
        const birthYear = this.birthday.split('.')[2];
        return `${firstNameInitial}${lastNameLower}${birthYear}`;
      },
    };
  
    newUser.setFirstName(prompt('Введіть ім\'я:'));
    newUser.setLastName(prompt('Введіть прізвище:'));
    newUser.setBirthday(prompt('Введіть дату народження (у форматі dd.mm.yyyy):'));
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log('Інформація про користувача:');
  console.log(user);
  console.log('Вік користувача:', user.getAge());
  console.log('Пароль користувача:', user.getPassword());
  